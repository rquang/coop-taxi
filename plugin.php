<?php
/*
Plugin Name:    CO-OP Taxi
Plugin URI:     https://socialhero.ca
Description:    Plugin for CO-OP Taxi
Version:        1.0.0
Author:         Social Hero Media
Author URI:     https://socialhero.ca
License:        GPL-2.0+
License URI:    http://www.gnu.org/licenses/gpl-2.0.txt

This plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

This plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with This plugin. If not, see {URI to Plugin License}.
 */

if (!defined('WPINC')) {
    die;
}

date_default_timezone_set('America/Edmonton');

// Writes to the Debug Log
if (!function_exists('write_log')) {
    function write_log($log)
    {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}

add_action('wp_enqueue_scripts', 'coop_enqueue_files');

$GLOBALS['google_api'] = "AIzaSyCZvwV5yFmLRgdcTKOExvpnS-Qp3aB9n6E";

function coop_enqueue_files()
{
    wp_enqueue_style('coop-style', plugin_dir_url(__FILE__) . 'assets/css/coop.css', '', filemtime(plugin_dir_url(__FILE__) . 'assets/css/coop.css'));
}

function essentials_enqueue_files()
{
    $userIP = getUserIP();
    $userIPmasked = str_replace(".", "Z", $userIP);

    wp_enqueue_style('bootstrap-style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap-datetimepicker-style', 'https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css');

    wp_enqueue_script('google-recaptcha', 'https://www.google.com/recaptcha/api.js', array('jquery'), null, false);
    wp_enqueue_script('google-maps', 'https://maps.google.com/maps/api/js?key=' . $GLOBALS['google_api'] . '&libraries=places&language=en-AU&sessiontoken=' . $userIPmasked, array('jquery'), null, true);

}

function booking_enqueue_files()
{
    wp_enqueue_style('bootstrap-style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
    wp_enqueue_style('fontawesome-style', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('bootstrap-datetimepicker-style', 'https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css');

    wp_enqueue_script('bootstrap-script', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), null, true);
    wp_enqueue_script('bootstrap-bundle-script', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js', array('jquery'), null, true);
    wp_enqueue_script('moment-script', plugin_dir_url(__FILE__) . 'assets/js/moment-with-locales.min.js', array('jquery'), null, true);
    wp_enqueue_script('bootstrap-datetimepicker', 'https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js', array('jquery'), null, true);
    wp_enqueue_script('jquery-validate', 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js', array('jquery'), null, true);

    wp_enqueue_script('booking-script', plugin_dir_url(__FILE__) . 'assets/js/booking.min.js', array('jquery'), '', true);
    // wp_enqueue_script('booking-script', plugin_dir_url( __FILE__ ) . 'src/js/booking.js' , array( 'jquery' ), '', true);
}

// Add admin action to make a booking
add_action( 'admin_post_booking', 'coop_admin_booking' );
add_action( 'admin_post_nopriv_booking', 'coop_admin_booking' );
function coop_admin_booking() {
    include "inc/api/submit.php";
}

function booking_form_shortcode()
{
    essentials_enqueue_files();
    booking_enqueue_files();
    include "inc/forms/booking.php";
}
add_shortcode('booking_form', 'booking_form_shortcode');

function airport_enqueue_files()
{
    wp_enqueue_script('airport-script', plugin_dir_url(__FILE__) . 'assets/js/airport.min.js', array('jquery'), '', true);
    // wp_enqueue_script('airport-script', plugin_dir_url( __FILE__ ) . 'src/js/airport.js' , array( 'jquery' ), '', true);
    wp_localize_script('airport-script', 'airportScript', array(
        'pluginsUrl' => plugins_url(),
    ));
}

function airport_form_shortcode()
{
    essentials_enqueue_files();
    airport_enqueue_files();
    include "inc/forms/airport.php";
}

add_shortcode('airport_form', 'airport_form_shortcode');

function boost_form_shortcode()
{
    essentials_enqueue_files();
    booking_enqueue_files();
    include "inc/forms/boost.php";
}

add_shortcode('boost_form', 'boost_form_shortcode');

function getUserIP()
{
    if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') > 0) {
            $addr = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
            return trim($addr[0]);
        } else {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    } else {
        return $_SERVER['REMOTE_ADDR'];
    }
}

add_filter('acf/load_field/name=data', 'acf_read_only');
function acf_read_only($field)
{
    $field['disabled'] = '1';
    return $field;
}

function disable_new_posts()
{
    // Hide sidebar link
    global $submenu;
    unset($submenu['edit.php?post_type=booking_log'][10]);

    // Hide link on listing page
    if (isset($_GET['post_type']) && $_GET['post_type'] == 'booking_log') {
        echo '<style type="text/css">
        #favorite-actions, .add-new-h2, .tablenav, .page-title-action, .subsubsub, .search-box, .row-actions { display:none; }
        </style>';
    }
}
add_action('admin_menu', 'disable_new_posts');

add_action( 'admin_menu', function () {
    remove_meta_box( 'submitdiv', 'booking_log', 'side' );
} );

// Disable Styling for Booking PT
function add_admin_scripts( $hook ) {

    global $post;

    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
        if ( 'booking_log' === $post->post_type ) {     
            echo '<style type="text/css">
            #favorite-actions, .add-new-h2, .tablenav, .page-title-action, .subsubsub, .search-box, .row-actions, .wp-heading-inline, #edit-slug-box, #postdivrich { 
                display:none !important; 
            }
            #title {
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                pointer-events: none;          
            }
            </style>';
        }
    }
}
add_action( 'admin_enqueue_scripts', 'add_admin_scripts', 10, 1 );

// Removes from admin menu
add_action( 'admin_menu', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function mytheme_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );