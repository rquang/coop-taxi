<?php
session_start(); 
include plugin_dir_path(dirname(__FILE__)) . 'api/IBSClass.php';
$vehicleTypes = getVehicleTypes(array('Login'=>'taxi', 'Password'=>'test'));
?>

<div class="container">
    <?php 
    include plugin_dir_path(dirname(__FILE__)) . 'forms/recaptcha.php';
    ?>
    <div id="coop" class="row border <?php if (!is_user_logged_in()): ?>d-none<?php endif; ?>">
        <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
            <form action="/wp-admin/admin-post.php" method="post" name="booking-form" id="booking-form" class="coop-form">
                <input type="hidden" name="action" value="booking" />
                <fieldset id="booking-form-wrap" <?php if (!is_user_logged_in()): ?>disabled<?php endif; ?>>
                    <input name="pickup_lat" id="pickup_lat" type="hidden">
                    <input name="pickup_long" id="pickup_long" type="hidden">
                    <input name="pickup_city" id="pickup_city" type="hidden">
                    <input name="pickup_street" id="pickup_street" type="hidden">
                    <input name="pickup_state" id="pickup_state" type="hidden">
                    <input name="pickup_zipcode" id="pickup_zipcode" type="hidden">
                    <input name="pickup_country" id="pickup_country" type="hidden">
                    <?php 
                    if (isset($vehicleTypes)) : 
                        foreach($vehicleTypes as $vehicleType) :
                            $thisVehicle = objectToObject($vehicleType, 'TVehicleTypeItem');
                            $vehicle_arr = ['Boost'];
                            if (in_array($thisVehicle->Name, $vehicle_arr)) : 
                            ?>
                                <input type="hidden" name="vehicle_type" id="vehicletype" value="<?php echo($thisVehicle->ID . ':' . $thisVehicle->Name); ?>">
                            <?php
                            endif;
                        endforeach;
                    else :
                        echo '<input type="hidden" name="vehicle_type" id="vehicletype" value="14:Boost"/>';
                    endif;
                    ?>
                    <div class="form-row">
                        <div class="col-12 col-sm form-group">
                            <label for="pickup">Address:<span class="form-req">*</span></label>
                            <input type="text" name="pickup" id="pickup" class="form-control" required>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col form-group">
                            <label for="fname">Full Name:<span class="form-req">*</span></label>
                            <input type="text" name="fname" id="fname" class="form-control" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-12 col-sm form-group">
                            <label for="phone">Phone Number:<span class="form-req">*</span></label>
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="" required>
                        </div>
                        <div class="col-12 col-sm form-group">
                            <label for="email">Email:<span class="form-req">*</span></label>
                            <input type="text" name="email" id="email" class="form-control" placeholder="" required>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col form-group">
                            <label name="additionalinst">Additional Instructions:</label>
                            <textarea name="additionalinst" id="additionalinst" class="form-control"></textarea>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row mb-4">
                        <div class="col form-check">
                            <label for="radio" class="d-block">Schedule Your Service:</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="radio" id="now" value="now" checked>
                                <label class="form-check-label" for="radio">Now</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="radio" id="later" value="later">
                                <label class="form-check-label" for="radio">Later</label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="booking_timestamp" id="booking_timestamp"/>
                    <div class="form-row">
                        <div class="col-12 col-sm form-group">
                            <div id="booking-date" class="input-append date time">
                                <label for="booking_date">Service Date:<span class="form-req">*</span></label>
                                <input type="text" class="form-control datetimepicker-input" name="booking_date" id="booking_date"
                                    data-toggle="datetimepicker" data-target="#booking_date" data-format="MM/DD/YYYY"
                                    placeholder="MM/DD/YYYY" required />
                            </div>
                        </div>
                        <div class="col-12 col-sm form-group">
                            <div id="booking-time" class="input-append date time">
                                <label for="booking_time">Service Time:<span class="form-req">*</span></label>
                                <input type="text" class="form-control datetimepicker-input" name="booking_time" id="booking_time"
                                    data-toggle="datetimepicker" data-target="#booking_time" data-format="HH:MM"
                                    placeholder="HH:MM" required />
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col">
                            <?php 
                        $x = time() - $_SESSION['CREATED'];
                        if (isset($_SESSION["m_booknow"]) && $x < 600 && !is_user_logged_in()): $_SESSION["m_booknow"]; ?>
                            <p>You have recently booked a boosting service. Please try again later.</p>
                            <?php else: ?>
                                <input type="submit" id="submit-btn" value="Book Now" class="btn btn-primary btn-lg w-100"/>
                                <span id="reset-btn" class="clear text-secondary float-right mt-2">Clear Form</span>
                            <?php endif; ?>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>