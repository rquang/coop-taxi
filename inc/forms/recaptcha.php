<?php if (!is_user_logged_in()): ?>
<div id="recaptcha" class="row">
    <div class="col border p-4 d-flex flex-column justify-content-center align-items-center">
        <h2>Security Verification</h2>
        <p>Before you book, we need to verify you're not a robot!</p>
        <p>Please complete this security check:</p>
        <div class="g-recaptcha" data-sitekey="6LfPNZkUAAAAAHJJmbZkZjT_qGZWlCZjqpnobPcz" data-callback="enableForm">
        </div>
    </div>
</div>
<?php endif; ?>