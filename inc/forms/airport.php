<div class="container">
    <?php 
    include plugin_dir_path(dirname(__FILE__)) . 'forms/recaptcha.php';
    ?>
    <div id="coop" class="row border <?php if (!is_user_logged_in()): ?>d-none<?php endif; ?>">
        <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
            <div id="pickaddress" class="gtaddress">
                <form method="get" class="coop-form">
                    <input type="hidden" name="latitude" id="latitude" />
                    <input type="hidden" name="longitude" id="longitude" />
                    <div class="form-row">
                        <div class="col form-group">
                            <label for="pickpoint_text">Pickup Address<span class="form-req">*</span></label>
                            <input class="autocomplete form-control" id="start" name="pickuppoint_text" placeholder="Enter your address" type="text" required/>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                        <input type="button" name="submit" id="fare-button" value="Calculate" class="btn btn-primary btn-lg w-100" id="btn_fn" />
                        </div>
                    </div>
                </form>
            </div>
            <div class="row mt-5">
                <div id="airport_fare" class="col-12 col-md-6 text-center mb-1" style="display:none;">
                    <span class="subheading">Flat Rate To Airport</span>
                    <div class="d-flex flex-row justify-content-center">
                        <span>$ </span>
                        <span id="airport_fare_total" class="airport-subtotal"></span>
                    </div>
                </div>
                <div id="normal_fare" class="col-12 col-md-6 text-center mb-1" style="display:none;">
                    <span class="subheading">Normal Fare</span>
                    <div class="d-flex flex-row justify-content-center">
                        <span>$ </span>
                        <span id="normal_fare_total" class="airport-subtotal"></span>
                    </div>
                </div>
                <div id="you_save" class="col-12 mt-3 border border-danger text-center" style="display:none;">
                    <h3 id="you_saved" style="display:none;">Your Savings</h3>
                    <div id="fare_savings"></div>   
                </div>
            </div>
        </div>
    </div>
</div>