<?php
session_start(); 
include plugin_dir_path(dirname(__FILE__)) . 'api/IBSClass.php';
$vehicleTypes = getVehicleTypes(array('Login'=>'taxi', 'Password'=>'test'));
?>

<div class="container">
    <?php 
    include plugin_dir_path(dirname(__FILE__)) . 'forms/recaptcha.php';
    ?>
    <div id="coop" class="row border <?php if (!is_user_logged_in()): ?>d-none<?php endif; ?>">
        <div class="col-lg-5 d-none d-md-block">
            <div id="location_results">
                <div class="maps" id="map_canvas"></div>
            </div>
            <div class="fare-result border rounded-0 p-3 mb-4">
                <div class="row">
                    <div class="col-12 form-group mb-1">
                        <label for="fare_distance" class="m-0">Distance(km): </label>
                        <input type="text" name="fare_distance" class="fare_distance font-weight-bold green form-control" readonly />
                    </div>
                    <div class="col-12 form-group mb-1">
                        <label for="fare_price" class="m-0">Price:</label>
                        <input type="text" name="fare_price" class="fare_price font-weight-bold green form-control" readonly />
                    </div>
                    <div class="col-12">
                        <small class="fare-disclaimer">Fare prices showed here are estimates only and do not reflect variations due to traffic delays or other factors. Actual meter fare may vary.</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <form action="/wp-admin/admin-post.php" method="post" name="booking-form" id="booking-form" class="coop-form">
                <input type="hidden" name="action" value="booking" />
                <fieldset id="booking-form-wrap" <?php if (!is_user_logged_in()): ?>disabled<?php endif; ?>>
                    <input name="pickup_lat" id="pickup_lat" type="hidden">
                    <input name="pickup_long" id="pickup_long" type="hidden">
                    <input name="pickup_city" id="pickup_city" type="hidden">
                    <input name="pickup_street" id="pickup_street" type="hidden">
                    <input name="pickup_state" id="pickup_state" type="hidden">
                    <input name="pickup_zipcode" id="pickup_zipcode" type="hidden">
                    <input name="pickup_country" id="pickup_country" type="hidden">
                    <input name="drop_lat" id="drop_lat" type="hidden">
                    <input name="drop_long" id="drop_long" type="hidden">
                    <input name="drop_city" id="drop_city" type="hidden">
                    <input name="drop_street" id="drop_street" type="hidden">
                    <input name="drop_state" id="drop_state" type="hidden">
                    <input name="drop_zipcode" id="drop_zipcode" type="hidden">
                    <input name="drop_country" id="drop_country" type="hidden">
                    <?php 
                    if (isset($vehicleTypes)) : 
                    ?>
                        <div class="form-row mb-4">
                            <div class="col form-check">
                                <label for="vehicle_type" class="d-block">Select Your Vehicle Type:</label>
                                <?php
                                foreach($vehicleTypes as $vehicleType) :
                                    $thisVehicle = objectToObject($vehicleType, 'TVehicleTypeItem');
                                    $vehicle_arr = ['Sedan', 'Minivan', 'Wheelchair Van'];
                                    if (in_array($thisVehicle->Name, $vehicle_arr)) : 
                                    ?>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="vehicle_type" value="<?php echo($thisVehicle->ID . ':' . $thisVehicle->Name); ?>" <?php if ($thisVehicle->Name == 'Sedan') echo('checked'); ?>>
                                            <label class="form-check-label" for="vehicle_type"><?php echo( $thisVehicle->Name); ?></label>
                                        </div>                               
                                    <?php
                                    endif;
                                endforeach;
                                ?>
                            </div>
                        </div>
                    <?php
                    else :
                        echo '<input type="hidden" name="vehicletype" id="vehicletype" value="165:Sedan"/>';
                    endif;
                    ?>
                    <hr>
                    <div class="form-row">
                        <div class="col-12 col-sm form-group">
                            <label for="pickup">Pickup Address:<span class="form-req">*</span></label>
                            <input type="text" name="pickup" id="pickup" class="form-control" required>
                        </div>
                        <div class="col-12 col-sm form-group">
                            <label for="dropoff">Destination Address:<span class="form-req">*</span></label>
                            <input type="text" name="dropoff" id="dropoff" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <input type="button" id="fare-btn" class="btn btn-secondary btn-lg w-100" value="Calculate fare estimate" />
                        </div>
                    </div>
                    <div class="form-row mt-4 d-md-none">
                        <div class="col-12 form-group mb-1">
                            <label for="fare_distance">Distance(km): </label>
                            <input type="text" name="fare_distance" class="fare_distance font-weight-bold green form-control" readonly />
                        </div>
                        <div class="col-12 form-group mb-1">
                            <label for="fare_price">Price:</label>
                            <input type="text" name="fare_price" class="fare_price font-weight-bold green form-control" readonly />
                        </div>
                        <div class="col-12">
                            <small class="fare-disclaimer">Fare prices showed here are estimates only and do not reflect variations due to traffic delays or other factors. Actual meter fare may vary.</small>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col form-group">
                            <label for="fname">Full Name:<span class="form-req">*</span></label>
                            <input type="text" name="fname" id="fname" class="form-control" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-12 col-sm form-group">
                            <label for="phone">Phone Number:<span class="form-req">*</span></label>
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="" required>
                        </div>
                        <div class="col-12 col-sm form-group">
                            <label for="email">Email:<span class="form-req">*</span></label>
                            <input type="text" name="email" id="email" class="form-control" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col form-group">
                            <label name="AptUnitBuzzer">Unit Buzzer:</label>
                            <input type="text" name="AptUnitBuzzer" id="AptUnitBuzzer" class="form-control"></input>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col form-group">
                            <label name="additionalinst">Additional Instructions:</label>
                            <textarea name="additionalinst" id="additionalinst" class="form-control"></textarea>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row mb-4">
                        <div class="col form-check">
                            <label for="radio" class="d-block">Choose your Pickup Date & Time:</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="radio" id="now" value="now" checked>
                                <label class="form-check-label" for="radio">Now</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="radio" id="later" value="later">
                                <label class="form-check-label" for="radio">Later</label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="booking_timestamp" id="booking_timestamp"/>
                    <div class="form-row">
                        <div class="col-12 col-sm form-group">
                            <div id="booking-date" class="input-append date time">
                                <label for="booking_date">Pickup Date:<span class="form-req">*</span></label>
                                <input type="text" class="form-control datetimepicker-input" name="booking_date" id="booking_date"
                                    data-toggle="datetimepicker" data-target="#booking_date" data-format="MM/DD/YYYY"
                                    placeholder="MM/DD/YYYY" required />
                            </div>
                        </div>
                        <div class="col-12 col-sm form-group">
                            <div id="booking-time" class="input-append date time">
                                <label for="booking_time">Pickup Time:<span class="form-req">*</span></label>
                                <input type="text" class="form-control datetimepicker-input" name="booking_time" id="booking_time"
                                    data-toggle="datetimepicker" data-target="#booking_time" data-format="HH:MM"
                                    placeholder="HH:MM" required />
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col">
                            <?php 
                        $x = time() - $_SESSION['CREATED'];
                        if (isset($_SESSION["m_booknow"]) && $x < 120 && !is_user_logged_in()): $_SESSION["m_booknow"]; ?>
                            <p>You have recently booked a taxi. Please try again later.</p>
                            <?php else: ?>
                                <input type="submit" id="submit-btn" value="Book Now" class="btn btn-primary btn-lg w-100"/>
                                <span id="reset-btn" class="clear text-secondary float-right mt-2">Clear Form</span>
                            <?php endif; ?>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>