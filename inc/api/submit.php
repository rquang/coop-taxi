<?php

$training_flag = false;
$debug_flag = false;

if( current_user_can('administrator') ) {
    if ( get_field('training_flag', 'options') ) {
        $training_flag = true;
    }
    if ( get_field('debug_flag', 'options') ) {
        $debug_flag = true;
    }
}

//Exit if accessed directly
if ( !defined('ABSPATH') ) {
    //If wordpress isn't loaded load it up.
    $path = $_SERVER['DOCUMENT_ROOT'];
    include_once $path . '/wp-load.php';
}

ob_start();
session_start();

include plugin_dir_path(dirname(__FILE__)) . 'api/IBSClass.php';
include plugin_dir_path(dirname(__FILE__)) . 'api/booking_log.php';

$_SESSION["m_booknow"] = "disabled";
$_SESSION['CREATED'] = time();

if ($_POST) {

    $pickupAddress = stripslashes($_POST['pickup']);

    // $latitude = floatval($_POST['latitude']);
    // $longitude = floatval($_POST['longitude']);

    $pickup_lat = $_POST['pickup_lat'];
    $pickup_long = $_POST['pickup_long'];
    $pickup_city = $_POST['pickup_city'];
    $pickup_street = $_POST['pickup_street'];
    $pickup_state = $_POST['pickup_state'];
    $pickup_zipcode = $_POST['pickup_zipcode'];
    $pickup_country = $_POST['pickup_country'];

    $dropoffAddress = stripslashes($_POST['dropoff']);

    $drop_lat = $_POST['drop_lat'];
    $drop_long = $_POST['drop_long'];
    $drop_city = $_POST['drop_city'];
    $drop_street = $_POST['drop_street'];
    $drop_state = $_POST['drop_state'];
    $drop_zipcode = $_POST['drop_zipcode'];
    $drop_country = $_POST['drop_country'];

    $fname = stripslashes($_POST['fname']);
    $email = stripslashes($_POST['email']);
    $phone = stripslashes($_POST['phone']);

    $AptUnitBuzzer = stripslashes($_POST['AptUnitBuzzer']);
    $vehicletype = explode(':', stripslashes($_POST['vehicle_type']));
    // $psnger = stripslashes($_POST['psnger']);
    // $pets = stripslashes($_POST['pets']);
    $additionalinst = stripslashes($_POST['additionalinst']);
    $additionalinst = "***Web Booking*** " . $additionalinst;
    $date = stripslashes($_POST['booking_date']);

    // $datepick = explode(" ", $date);

    $date_break = explode("/", $date);
    $month = $date_break[0];
    $month = intval(ltrim($month, '0'));
    $days = intval($date_break[1]);
    $years = intval($date_break[2]);

    $vehicleID = intval($vehicletype[0]);
    $vehicleName = $vehicletype[1];
    
    $time = stripslashes($_POST['booking_time']);
    $time_break = explode(" ", $time);
    $meridiem = $time_break[1];
    $time_break = explode(":", $time_break[0]);
    $hour = intval($time_break[0]);
    $minutes = intval($time_break[1]);
    
    if ($meridiem == 'PM') {
        $hour += 12;
    } elseif ($meridiem == 'AM' && $hour == 12) {
        $hour = 0;
    }

    $accountParams = array(
        'Login' => 'taxi',
        'Password' => 'test'
    );

    $myAccount = getAccount($accountParams);
    $myAccountID = $myAccount->AccountID;

    if ($debug_flag) {
        print('<hr/><h1>calling IStaticData/GetAccount</h1>');
        print "<pre>";
        print_r($myAccount);
        print "</pre>";
        print('<hr/><h1>calling IStaticData/GetProviders</h1>');
    }

    if ($training_flag) {
        $myProvider = getTRNProvider($accountParams);
    } else {
        $myProvider = getDefaultProvider($accountParams);
    }
    if ($debug_flag) {
        print "<pre>";
        print_r($myProvider);
        print "</pre>";
    }

    /* print('<hr/><h1>calling IStaticData/GetPickupCityList</h1>');
    $myPickupCities = getPickupCityList($accountParams);
    var_dump($myPickupCities);print('<hr/>');
    foreach($myPickupCities as $myPickupCitiess)
    {

    // $myPickupCitiess->CityID
    // $myPickupCitiess->Name

    echo $myPickupCitiess->CityID;
    echo $myPickupCitiess->Name."<br/>";
    } */

    // print('<hr/><h1>calling IStaticData/GetVehicleTypes</h1>');

    // $myVehicleList = getVehicleTypes($accountParams);
    // print "<pre>";
    // print_r($myVehicleList);
    // print "</pre>";

    /*
    print('<hr/><h1>calling IStaticData/GetDropoffCityList</h1>');
    $myDropoffCities = getDropoffCityList($accountParams);
    var_dump($myDropoffCities);print('<hr/>'); */

    // include("wp-config.php");
    // $Date = date("m/d/Y") ;

    /* print strftime('%c');
    $query= "INSERT INTO `actaxi_taxi_wp`.`wp_ibsform` (`myAccount`, `myProviderid`, `myVehicleList`, `update_date`) VALUES ('$myAccountID', '$myProviders', '$myVehicleLists1', '')"; */

    // "insert into wp_quote (name, email, phone, enquiry_details) values ('$name', '$email', '$phone', '$enquiry_details')";

    /* $insert=mysqli_query($query)or die('Query failed'.mysqli_error()); */

    // $date = date('Y-m-d');
    // $sql = "UPDATE  `actaxi_taxi_wp`.`wp_ibsform` SET  `myAccount` =  '$myAccountID',`myProviderid` =  '$myProviders',`myVehicleList` =  '$myVehicleLists1',`update_date` =  '$date' WHERE  `wp_ibsform`.`id` ='1'";
    // $result = mysqli_query($sql);
    // Check database for values
    // if value was saved more than 3 days ago or value does not exist in database
    // then update the database by calling above functions
    // then read the accountID, providerID and the list of vehicles from the database
    // print('<hr/><h1>calling IWEBOrder_7/SaveBookOrder_7</h1>');


    // $pickupLatLong = lookup($pickupAddress);

    $tPickup = new TWEBAddress();
    $tPickup->StreetPlace = $pickupAddress;
    // $tPickup->Latitude = $latitude;
    // $tPickup->Longitude = $longitude;
    $tPickup->AptBaz = $AptUnitBuzzer;
    $tPickup->Postal = $pickup_zipcode;
    $tPickup->Latitude = $pickup_lat;
    $tPickup->Longitude = $pickup_long;

    // if($pickupLatLong != null) {
    //     $tPickup->Latitude = $pickupLatLong['latitude'];
    //     $tPickup->Longitude = $pickupLatLong['longitude'];
    // }

    // $dropoffLatLong = lookup($dropoffAddress);

    $tDropOff = new TWEBAddress();
    $tDropOff->StreetPlace = $dropoffAddress;
    $tDropOff->AptBaz = null;
    $tDropOff->Postal = $drop_zipcode;
    $tDropOff->Latitude = $drop_lat;
    $tDropOff->Longitude = $drop_long;

    // if ($dropoffLatLong != null) {
    //     $tDropOff->Latitude = $dropoffLatLong['latitude'];
    //     $tDropOff->Longitude = $dropoffLatLong['longitude'];
    // }

    $tPickupDate = new TWEBTimeStamp();
    $tPickupDate->Year = $years;
    $tPickupDate->Month = $month;
    $tPickupDate->Day = $days;
    $tPickupTime = new TWEBTimeStamp();
    $tPickupTime->Hour = $hour;
    $tPickupTime->Minute = $minutes;
    
    $orderDate = new DateTime();
    $tOrderDate = new TWEBTimeStamp();
    $tOrderDate->Year = $orderDate->format('Y');
    $tOrderDate->Month = $orderDate->format('m');
    $tOrderDate->Day = $orderDate->format('d');
    $tOrderDate->Hour = $orderDate->format('H');
    $tOrderDate->Minute = $orderDate->format('i');
    $tOrderDate->Second = $orderDate->format('s');

    $tBookOrder = new TBookOrder_7();
    $tBookOrder->AccountID = 1;
    // $tBookOrder->AccountID = $myAccount->AccountID;
    $tBookOrder->ServiceProviderID = $myProvider->ProviderNum;
    // $tBookOrder->ServiceProviderID = $myProvider->ProviderID;
    $tBookOrder->Customer = $fname;
    // $tBookOrder->AptUnitBuzzer = $_POST['AptUnitBuzzer'];
    $tBookOrder->Phone = $phone;
    $tBookOrder->PickupDate = $tPickupDate;
    $tBookOrder->PickupTime = $tPickupTime;
    $tBookOrder->PickupAddress = $tPickup;
    $tBookOrder->DropoffAddress = $tDropOff;
    $tBookOrder->ContactPhone = $phone;
    $tBookOrder->OrderDate = $tOrderDate;
    // $tBookOrder->Passengers = $_POST['psnger'];
    // $tBookOrder->pets = $_POST['pets'];
    $tBookOrder->email = $email;
    $tBookOrder->Note = $additionalinst;
    $tBookOrder->OrderedBy = 'Website';
    $tBookOrder->ChargeTypeID = 1;
    $tBookOrder->VehicleTypeID = $vehicleID;
    $tBookOrder->VehicleTypeName = $vehicleName;
    $tBookOrder->OrderStatus = TWEBOrderStatusValue::wosPost;

    $orderParams = array(
        'Login' => 'taxi',
        'Password' => 'test',
        'BookOrder' => $tBookOrder
    );
    $myOrder = createOrder($orderParams);

    if ($debug_flag) {
        print('<hr/><h1>calling IStaticData/TBookOrder</h1>');
        print "<pre>";
        print_r($tBookOrder);
        print "</pre>";
        print '<br/>';
        // exit;
        print('<hr/><h1>calling IStaticData/CreateOrder</h1>');
        print_r($myOrder);
        print '<hr/>';
    } else {
        if ($myOrder > 0) {
            booking_log_create_post($tBookOrder, $myOrder);
            include plugin_dir_path(dirname(__FILE__)) . 'api/mail.php';
            header('Location: ' . get_home_url() . '/thank-you/');
        } else {
            header('Location: ' . get_home_url() . '/error/');
        }
    }
}
else {
    header("Location: https://co-optaxi.com/error/");
}

?>