<?php

/**
 * A function used to programmatically create a post in WordPress. The slug, author ID, and title
 * are defined within the context of the function.
 *
 * @returns -1 if the post was never created, -2 if a post with the same title exists, or the ID
 *          of the post if successful.
 */
function booking_log_create_post($tBookOrder, $myOrder)
{

    // Initialize the page ID to -1. This indicates no action has been taken.
    $post_id = -1;

    // Setup the author, slug, and title for the post
    $user = get_user_by('slug', 'quang');
    $author_id = $user->ID;
    $slug = $myOrder;
    $title = $myOrder;

    // If the page doesn't already exist, then create it
    if (null == get_page_by_title($title)) {
        // Set the post ID so that we know the post was created successfully
        $post_id = wp_insert_post(
            array(
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_author' => $author_id,
                'post_name' => $slug,
                'post_title' => $title,
                'post_status' => 'publish',
                'post_type' => 'booking_log',
            )
        );
        update_field('data', var_export($tBookOrder, true), $post_id);
    } else {
        // Arbitrarily use -2 to indicate that the page with the title already exists
        $post_id = -2;
    }

}
