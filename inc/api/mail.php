<?php
$to = $email;
$subject = "Booking Details Co-op taxi";
$message = '
<table width="500" border="0" align="left" style="padding:0px 0px 0px 10px;">
    <tr>
        <th align="left" scope="col" colspan="2"><h3>Thank you for booking with us! Here are your booking details!<br/><br/></h3></th>
    </tr>
    <tr></tr>
    <tr>
        <td scope="col" width="150"><b> Order ID:</b>&nbsp;</td>
        <td scope="col" width="350">' . $myOrder . '</td>
    </tr>
    <tr>
        <td scope="col" width="150"><b> Name:</b>&nbsp;</td>
        <td scope="col" width="350">' . $fname . '</td>
    </tr>
    <tr>
        <td scope="col" width="150"><b>Email:</b>&nbsp;</td>
        <td scope="col" width="350">' . $email . '</td>
    </tr>
    <tr>
        <td scope="col" width="150"><b>Phone:</b>&nbsp;</td>
        <td scope="col" width="350">' . $phone . '</td>
    </tr>
    <tr>
        <td scope="col" width="150"><b>Pickup Point:</b>&nbsp;</td>
        <td scope="col" width="350">' . $pickupAddress . '</td>
    </tr>
    <tr>
        <td scope="col" width="150"><b>Pickup Date:</b>&nbsp;</td>
        <td scope="col" width="350">' . $date . '</td>
    </tr>
    <tr>
        <td scope="col" width="150"><b>Pickup Time:</b>&nbsp;</td>
        <td scope="col" width="350">' . $time . '</td>
    </tr>
    <tr><br/><br/></tr>
    <tr>
        <th align="left" scope="col" colspan="2"><h3><br/><br/> For more help, call us at 780-425-2525!</h3></th>
    </tr>
    <tr>
        <th align="left" scope="col" colspan="2"><h3><br/>Team<br/>Co-op Taxi<br/></h3></th>
    </tr>
</table>';

// If there is no error, send the email

$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=iso-8859-1';
$headers[] = 'From: CO-OP TAXI <donotreply@co-optaxi.com>';
$headers[] = 'Reply-To: reception@co-optaxi.com';
wp_mail($to, $subject, $message, $headers);

// $headers = 'MIME-Version: 1.0' . "\r\n";
// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
// $headers .= 'From: Co-op taxi<dispatcher@co-optaxi.com>' . "\r\n";
// 'X-Mailer: PHP/' . phpversion();
// mail($to, $subject, $message, $headers);
