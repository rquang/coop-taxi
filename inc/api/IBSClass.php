<?php
define('wsdl', 'http://abcoop.dyndns.org:6928');
define("google_map_address", "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyChNWg4ddLv7CKMu6ryPSiUNM7mwFgBi6M&sensor=false&address=");
define("wsdl_static_address", wsdl . "/xds_iaspi.dll/wsdl/IStaticData"); //xds_iaspi_V19.dll/wsdl/IStaticData// http://checkerjax.dyndns-ip.com:6923, http://abcoop.dyndns.org:6928
define("wsdl_order_address", wsdl . "/xds_iaspi.dll/wsdl/IWEBOrder_7"); //xds_iaspi_V19.dll/wsdl/IWEBOrder_7//http://abcoop.dyndns.org:6928/xds_iaspi.dll/wsdl/IWEBOrder_7

class TBookOrder
{
    public $ServiceProviderID = null; //int
    public $ConfirmationID = null; //int
    public $AccountID = null; //int
    public $Customer = ''; //string
    public $Phone = ''; //string
    public $PickupDate = null; //TWEBTimeStamp
    public $PickupTime = null; //TWEBTimeStamp
    public $PickupAddress = null; //TWEBAddress
    public $DropoffAddress = null; //TWEBAddress
    public $Passengers = 0; //int
    public $VehicleTypeName = null; //string
    public $VehicleTypeID = null; //int
    public $Note = ''; //string
    public $ContactPhone = ''; //string
    public $Fare = 0.00; //double
    public $Tolls = 0.00; //double
    public $EstDistance = 0.00; //double
    public $CCNumber = ''; //string
    public $CCExpMonth = ''; //string
    public $CCExpYear = ''; //string
    public $CCTransID = null; //int
    public $CCHolder = ''; //string
    public $CCStartMonth = ''; //string
    public $CCStartYear = ''; //string
    public $CCSecurityCode = ''; //string
    public $CCIssueNumber = ''; //string
    public $CCTransDate = ''; //string
    public $CCTransResult = ''; //TCCTransResult
    public $CCRespondText = ''; //string
    public $CCAuth = ''; //string
    public $OrderStatus = ''; //TWEBOrderStatusValue
    public $OrderID = null; //int
    public $OrderDate = null; //TWEBTimeStamp
}
class TBookOrder_2 extends TBookOrder
{
    //additional fields for 2
    public $OrderedBy = null; //string
    public $MajorIntersect = ''; //string
    public $CancelNum = 0; //int
    public $Airline = ''; //string
    public $Flight = ''; //string
    public $Arrival = null; //TWEBTimeStamp
    public $AirportCode = ''; //string
    public $CrewAuthoriz = ''; //string
    public $PermanentRem = ''; //string
}
class TBookOrder_3 extends TBookOrder_2
{
    //additional fields for 3
    public $RideStops = null; //TRideStopArray
    public $TxtBack = false; //boolean
    public $EmailBack = false; //boolean
    public $CallBack = false; //boolean
    public $Prompt1 = ''; //string
    public $Prompt2 = ''; //string
    public $Prompt3 = ''; //string
    public $Prompt4 = ''; //string
    public $Prompt5 = ''; //string
    public $Prompt6 = ''; //string
    public $Prompt7 = ''; //string
    public $Prompt8 = ''; //string
    public $Field1 = ''; //string
    public $Field2 = ''; //string
    public $Field3 = ''; //string
    public $Field4 = ''; //string
    public $Field5 = ''; //string
    public $Field6 = ''; //string
    public $Field7 = ''; //string
    public $Field8 = ''; //string
    public $TxtBackAdd = ''; //string
    public $EmailBackAdd = ''; //string
    public $CallBackNo = ''; //string
    public $CCAddress = ''; //string
    public $CCPostCode = ''; //string
    public $RideID = null; //int
}
class TBookOrder_4 extends TBookOrder_3
{
    //additional fields for 4
    public $ccType = ''; //string
    public $ccFirst = ''; //string
    public $ccLast = ''; //string
    public $ccCity = ''; //string
    public $ccState = ''; //string
}
class TBookOrder_5 extends TBookOrder_4
{
    //additional fields for 5
    public $ChargeTypeID = 2; //int
    public $RideExceptions = null; //TExceptionsArray

    public $CabNo = ''; //string
    public $CallNumber = ''; //string
    public $DispByAuto = false; //boolean
    public $FrCity = ''; //string
    public $FrRegion = ''; //string
    public $ToCity = ''; //string
    public $ToRegion = ''; //string
}
class TBookOrder_6 extends TBookOrder_5
{
    //additional fields for 6
    public $Priority = 1; //int
}
class TBookOrder_7 extends TBookOrder_6
{
    //additional fields for 7
    public $Tips = 0.00; //double
    public $VAT = 0.00; //double
}
class TRideStopArray
{

}
class TExceptionsArray
{

}
class TWEBOrderStatusValue
{
    const wosPost = "wosPost";
}
class TWEBTimeStamp
{
    public $Year; //int
    public $Month; //int
    public $Day; //int
    public $Hour; //int
    public $Minute; //int
    public $Second; //int
    public $Fractions; //int
}
class TWEBAddress
{
    public $StreetPlace = ''; //string
    public $AptBaz = ''; //string
    public $Postal = ''; //string
    public $CityID = -1; //int
    public $RegionID = -1; //int
    public $CountryID = -1; //int
    public $Longitude = 0.00; //double
    public $Latitude = 0.00; //double
    public $AddressID = -1; //int
    public $StopTime = null; //new TWEBAddress(); //TWEBTimeStamp
    public $StopOrder = 0; //int
    public $RunOrder = 0; //int
    public $StopPass = 0; //int
    public $Building_name = ''; //string
}
class TProviderItem
{
    public $ProviderName = ''; //string
    public $ProviderID = ''; //string
    public $ProviderNum = -1; //integer
    public $isDefault = false; //boolean
}
class TVehicleTypeItem
{
    public $Name = ''; //string
    public $Capacity = ''; //string
    public $ID = -1; //int
    public $isDefault = false; //boolean
}
class TCityItem
{
    public $CityID; //int;
    public $Name; //string
}
function objectToObject($instance, $className)
{
    return unserialize(sprintf(
        'O:%d:"%s"%s',
        strlen($className),
        $className,
        strstr(strstr(serialize($instance), '"'), ':')
    ));
}
function lookup($string)
{
    $string = str_replace(" ", "+", urlencode($string));
    $details_url = google_map_address . $string;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $details_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = json_decode(curl_exec($ch), true);

    // echo '<pre>';
    // print_r($response);
    // echo '</pre>';
    // If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
    if ($response['status'] != 'OK') {
        return null;
    }

    $geometry = $response['results'][0]['geometry'];

    $longitude = $geometry['location']['lat'];
    $latitude = $geometry['location']['lng'];

    $array = array(
        'latitude' => $geometry['location']['lat'],
        'longitude' => $geometry['location']['lng'],
        'location_type' => $geometry['location_type'],
    );
    return $array;

}

function getAccount($param)
{
    $wsdlStatic = wsdl_static_address;
    $opts = array('http' => array('protocol_version' => '1.0'));
    $context = stream_context_create($opts);
    $options = array('exceptions' => 0,
        'trace' => 1,
        // 'stream_context' => $context,
        'cache_wsdl' => WSDL_CACHE_NONE);
    //$this->client = new SoapClient($wsdl, array('stream_context' => $context));
    $staticData = new SoapClient($wsdlStatic, $options);
    $account = $staticData->__soapcall("GetAccount", $param);
    if (is_soap_fault($account)) {
        var_dump($account->faultcode);
        var_dump($account->faultstring);
        return null;
    }
    $staticData = null;
    return $account;
}
function getProviders($param)
{
    $wsdlStatic = wsdl_static_address;
    $options = array('exceptions' => 0,
        'trace' => 1,
        'cache_wsdl' => WSDL_CACHE_NONE);
    $staticData = new SoapClient($wsdlStatic, $options);
    $providers = $staticData->__soapcall("GetProviders", $param);
    if (is_soap_fault($providers)) {
        var_dump($providers->faultcode);
        var_dump($providers->faultstring);
        return null;
    }
    $staticData = null;
    return $providers;
}

function getTRNProvider($param)
{
    $providers = getProviders($param);
    $provider = null;
    foreach ($providers as $item) {
        $provider = objectToObject($item, 'TProviderItem');
        if ($provider->isDefault != true) { //return TRN provider
            return $provider;
        }
    }
    return null;
}

function getDefaultProvider($param)
{
    $providers = getProviders($param);
    $provider = null;
    foreach ($providers as $item) {
        $provider = objectToObject($item, 'TProviderItem');
        if ($provider->isDefault == true) { //return first provider
            return $provider;
        }
    }
    return null;
}

function getPickupCityList($param)
{
    $wsdlStatic = wsdl_static_address;
    $options = array('exceptions' => 0,
        'trace' => 1,
        'cache_wsdl' => WSDL_CACHE_NONE);
    $staticData = new SoapClient($wsdlStatic, $options);
    $pickupCityList = $staticData->__soapcall("GetPickupCityList", $param);
    if (is_soap_fault($pickupCityList)) {
        var_dump($pickupCityList->faultcode);
        var_dump($pickupCityList->faultstring);
        return null;
    }
    $pickupCityArray = array();
    foreach ($pickupCityList as $city) {
        $pickupCityArray[] = objectToObject($city, 'TCityItem');
    }
    $staticData = null;
    return $pickupCityArray;
}

function getDropoffCityList($param)
{
    $wsdlStatic = wsdl_static_address;
    $options = array('exceptions' => 0,
        'trace' => 1,
        'cache_wsdl' => WSDL_CACHE_NONE);
    $staticData = new SoapClient($wsdlStatic, $options);
    $dropoffCityList = $staticData->__soapcall("GetDropoffCityList", $param);
    if (is_soap_fault($dropoffCityList)) {
        var_dump($dropoffCityList->faultcode);
        var_dump($dropoffCityList->faultstring);
        return null;
    }
    $dropoffCityArray = array();
    foreach ($dropoffCityList as $city) {
        $dropoffCityArray[] = objectToObject($city, 'TCityItem');
    }
    $staticData = null;
    return $dropoffCityArray;
}

function getVehicleTypes($param)
{
    $wsdlStatic = wsdl_static_address;

    $content = @file_get_contents($wsdlStatic);
    if (!strpos($http_response_header[0], "200")) {
        return null;
    }

    $options = array('exceptions' => 0,
        'trace' => 1,
        'cache_wsdl' => WSDL_CACHE_NONE);
    $staticData = new SoapClient($wsdlStatic, $options);
    $vehicleTypes = $staticData->__soapcall("GetVehicleTypes", $param);
    if (is_soap_fault($vehicleTypes)) {
        var_dump($vehicleTypes->faultcode);
        var_dump($vehicleTypes->faultstring);
        return null;
    }
    $vehiclesArray = array();
    foreach ($vehicleTypes as $vehicleType) {
        $vehiclesArray[] = objectToObject($vehicleType, 'TVehicleTypeItem');
    }
    $staticData = null;
    return $vehiclesArray;
}

function createOrder($param)
{
    // echo '<pre>'; print_r($param); echo '</pre>';
    $wsdlOrder = wsdl_order_address;
    $options = array('exceptions' => 0,
        'trace' => 1,
        'cache_wsdl' => WSDL_CACHE_NONE);
    $order = new SoapClient($wsdlOrder, $options);
    $result = $order->__soapCall('SaveBookOrder_7', $param);

    if (is_soap_fault($result)) {
        var_dump($result->faultcode);
        var_dump($result->faultstring);
        return null;
    }
    if ($result > -1) {
        return $result;
    } else {
        print($result);
        print($result->faultcode);
        print($result->faultstring);
        return null;        
    }
}

function getZoneByGPS($param)
{
    $wsdlStatic = wsdl_static_address;
    $options = array('exceptions' => 0,
        'trace' => 1,
        'cache_wsdl' => WSDL_CACHE_NONE);
    $staticData = new SoapClient($wsdlStatic, $options);
    $zones = $staticData->__soapcall("GetZoneByGPS", $param);
    if (is_soap_fault($zones)) {
        var_dump($zones->faultcode);
        var_dump($zones->faultstring);
        return null;
    }
    $staticData = null;
    return $zones;
}

function shutDownFunction()
{
    $error = error_get_last();
    // fatal error, E_ERROR === 1
    if ($error['type'] === E_ERROR) {
        error_log(print_r($error, true));
        echo ("<h3>Unfortunately, we are experiencing technical difficulties in processing your online order. Sorry for the inconvenience caused, please call <a href='tel:7804252525'>780.425.2525</a> to order a cab.</h3>");
    }
}
register_shutdown_function('shutDownFunction');
