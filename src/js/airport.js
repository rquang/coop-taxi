function enableForm() {
    document.getElementById("coop").classList.remove("d-none");
    document.getElementById("recaptcha").outerHTML = "";
    document.getElementById('booking-form-wrap').disabled = false;
}

(function ($) {
    var autocomplete = {};
    var map = null;
    var directionDisplay;
    var directionsService = new google.maps.DirectionsService();
    var autocompletesWraps = ['pickaddress', 'test2'];
    var stateBounds = {
        ab: ["60.00006209999999", "-109.9998551", "48.9966671", "-120.0005219"]
        // lat/long boundaries list for states/provinces.
    };
    var autoOptions = {
        // types: ['address'],
        componentRestrictions: {
            country: 'CA'
        },
        bounds: getStateBounds('ab') // get LatLngBounds for ca.
    };

    google.maps.event.addDomListener(window, 'load', initialize);

    initialize();

    var autoCompleteFlag = false;

    $(document).on("keypress", "form", function (event) {
        return event.keyCode != 13;
    });

    $("#fare-button").on('click', function () {
        calcRoute();
        return false;
    });

    if (document.layers) {
        document.captureEvents(Event.KEYDOWN);
    }
    document.onkeydown = function (evt) {
        var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
        if (keyCode == 13) {
            // For Enter.
            calcRoute();
        }
    };

    $('#start').focus(function () {
        if (!autoCompleteFlag) {
            $(this).val("");
            autoCompleteFlag = true;
        }
        $(this).attr('autocomplete', 'new-password');
    });

    function getHTTPObject() {
        if (window.ActiveXObject)
            return new ActiveXObject("Microsoft.XMLHTTP");
        else if (window.XMLHttpRequest)
            return new XMLHttpRequest();
        else {
            alert("Your browser does not support AJAX.");
            return null;
        }
    }

    function getStateBounds(state) {
        return new google.maps.LatLngBounds(
            new google.maps.LatLng(stateBounds[state][0],
                stateBounds[state][1]),
            new google.maps.LatLng(stateBounds[state][2],
                stateBounds[state][3])
        );
    }

    function initialize() {
        directionsDisplay = new google.maps.DirectionsRenderer();
        // var mapOptions = {
        //     zoom: 7,
        //     mapTypeId: google.maps.MapTypeId.ROADMAP,
        //     center: new google.maps.LatLng(53.54, -113.49)
        // };
        // map = new google.maps.Map(document.getElementById('map_canvas'),
        //     mapOptions);


        //Find From location    
        var fromText = document.getElementById('start');
        var fromAuto = new google.maps.places.Autocomplete(fromText, autoOptions);

        google.maps.event.addListener(fromAuto, 'place_changed', function () {
            var place = fromAuto.getPlace();
            // document.getElementById('city').value = place.name;
            document.getElementById('latitude').value = place.geometry.location.lat();
            document.getElementById('longitude').value = place.geometry.location.lng();
        });
    }

    function calcRoute() {
        directionsDisplay = new google.maps.DirectionsRenderer();
        var start = document.getElementById('start').value,
            end = '1000 Airport Rd, EDMONTON INTERNATIONAL AIRPORT, AB T9E 0V3',
            request = {
                origin: start,
                destination: end,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                // Ajax code		
                var url = airportScript.pluginsUrl + "/coop-taxi/inc/api/action.php?cid=" + encodeURIComponent(start);
                request = getHTTPObject();
                // console.log(start);
                // console.log(url);
                request.open("GET", url, true);
                request.onreadystatechange = function () {
                    if (request.readyState == 4) {
                        var abc = request.responseText;
                        $('#airport_fare').fadeOut();
                        $('#normal_fare').fadeOut();
                        $('#you_saved').fadeOut();
                        if ( abc != 'METERED') {
                            if (/\d/.test(abc))
                                abc = abc + '.00';
                            $('#airport_fare_total').html(abc);
                            computePrice(computeTotalDistance(response));
                            computeDifference();
                        } else {
                            $('#fare_savings').html("Sorry we don't have a flat rate to airport from this location");
                            $('#you_save').fadeIn();
                        }
                    }
                };
                request.send();
            }
        });
    }

    function computeTotalDistance(result) {
        var total = 0;
        var myroute = result.routes[0];
        for (i = 0; i < myroute.legs.length; i++) {
            total += myroute.legs[i].distance.value;
        }
        return total;
    }

    function computePrice(distance) {
        /*  var meter = 1000;
            var distance = 135;
            var cent =.20;
            var cal = (meter/distance)*cent;
            var totaldist = cal.toFixed(3);*/
        var total = distance / 1000;
        //alert(total);
        //document.getElementById("total_price_main").innerHTML = total + " km";
        // document.getElementById('total_price_main').style.display = 'none';
        // document.getElementById('normal_fare').style.display = 'block';
        totalprice = ((total * 1.5) + 3.60);
        totalprice = Math.round(totalprice / 0.20) * 0.20;
        $("#normal_fare_total").html(totalprice.toFixed(2));
    }

    function computeDifference() {
        var $normal_fare = $('#normal_fare_total'),
            $airport_fare = $('#airport_fare_total'),
            $savings = $('#fare_savings'),
            difference = (parseFloat($normal_fare.html()) - parseFloat($airport_fare.html())).toFixed(2);
        console.log($normal_fare.html());
        console.log($airport_fare.html());
        console.log(difference);
        if (difference <= 0) {
            $savings.html("Sorry we don't have a flat rate to airport from this location");
        } else {
            if ($airport_fare.html() == '') {
                $savings.html("Only flat rate to airport available");
            } else {
                $('#airport_fare').fadeIn();
                $('#normal_fare').fadeIn();
                $('#you_saved').fadeIn();
                $savings.html("*$ " + difference);
            }
        }
        $('#you_save').fadeIn();
    }
})(jQuery);