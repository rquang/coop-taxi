function enableForm() {
    document.getElementById("coop").classList.remove("d-none");
    document.getElementById("recaptcha").outerHTML = "";
    document.getElementById('booking-form-wrap').disabled = false;
}

(function ($) {

    if ($("#map_canvas").length > 0) {
        var map = null,
            directionsDisplay = new google.maps.DirectionsRenderer(),
            directionsService = new google.maps.DirectionsService(),
            mapOptions = {
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: new google.maps.LatLng(53.54, -113.49)
            };
        // google.maps.event.addDomListener(window, 'load', initialize);
        map = new google.maps.Map(document.getElementById('map_canvas'),
            mapOptions);
    }

    var stateBounds = {
            ab: ["60.00006209999999", "-109.9998551", "48.9966671", "-120.0005219"]
            // lat/long boundaries list for states/provinces.
        },
        autoOptions = {
            // types: ['address'],
            componentRestrictions: {
                country: 'CA'
            },
            bounds: getStateBounds('ab') // get LatLngBounds for ca.
        };

    autoCompleteInput();

    var autoCompleteFlag = false,
        autoCompleteFlag1 = false;

    $.validator.addMethod("phoneUS", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
    }, "Please specify a valid phone number");

    $("#booking-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                phoneUS: true
            }
        }
    });

    $(document).on("keypress", "form", function (event) {
        return event.keyCode != 13;
    });

    $('#pickup').focus(function () {
        if (!autoCompleteFlag) {
            $(this).val("");
            autoCompleteFlag = true;
        }
        $(this).attr('autocomplete', 'new-password');
    });

    $('#dropoff').focus(function () {
        if (!autoCompleteFlag1) {
            $(this).val("");
            autoCompleteFlag1 = true;
        }
        $(this).attr('autocomplete', 'new-password');
    });

    //date time picker
    $('#booking_date').datetimepicker({
        format: 'L',
        minDate: moment()
    });
    $('#booking_time').datetimepicker({
        format: 'LT'
    });

    $("#now").on('click', function () {
        var cur_date = moment().format("L");
        var cur_time = moment().format("LT");
        $('#booking_date').val(cur_date).prop('readonly', true);
        $('#booking_time').val(cur_time).prop('readonly', true);
    });
    $("#now").trigger('click');

    $("#later").on('click', function () {
        $('#booking_date').prop('readonly', false);
        $('#booking_time').prop('readonly', false);
    });

    $("input[name='vehicle_type']").on('click', function () {
        var $this = $(this);
        var $fare = $(".fare_price");
        var farePrice = parseFloat($fare.data('fare'));
        if ($this.val().indexOf('Minivan') >= 0 && !$fare.hasClass('is--minivan')) {
            farePrice += 10;
            $fare.addClass('is--minivan');
        } else if ($fare.hasClass('is--minivan')) {
            $fare.removeClass('is--minivan');
        }
        if ($fare.val() != '') {
            $fare.val("$" + Math.floor(farePrice - 1).toFixed(2) + " - $" + Math.ceil(farePrice + 3).toFixed(2));
        } else {
            return;
        }
    });

    $("#booking_timestamp").val(moment().format("LT"));

    $("#fare-btn").on('click', calcRoute);

    $("#reset-btn").click(function (e) {
        $("#booking-form")[0].reset();
    });

    $("#submit-btn").click(function (e) {
        e.preventDefault(); //prevent default form submit
        if ($("#booking-form").valid()) {
            $(this).prop('disabled', true);
            $("#reset-btn").prop('disabled', true);
            // $('#additionalinst').val($('#additionalinst').val() + '*** Booking from co-op taxi website ***');
            $("#booking-form").submit();
        }
    });

    function compilePlace(place) {
        var address = place.formatted_address;
        var value = address.split(",");
        var location = [];
        var count = value.length;

        location.street = value[0].trim();
        location.country = value[count - 1].trim();

        var state = value[count - 2];
        var z = state.split(" ");
        var i = z.length;
        location.state = z[1].trim();
        if (i > 2)
            location.zipcode = z[2] + z[3].trim();

        location.city = value[count - 3].trim();
        location.lat = place.geometry.location.lat();
        location.lng = place.geometry.location.lng();
        return location;
    }

    function getStateBounds(state) {
        return new google.maps.LatLngBounds(
            new google.maps.LatLng(stateBounds[state][0],
                stateBounds[state][1]),
            new google.maps.LatLng(stateBounds[state][2],
                stateBounds[state][3])
        );
    }

    function autoCompleteInput() {
        //Find From location    
        var fromText = document.getElementById('pickup'),
            fromAuto = new google.maps.places.Autocomplete(fromText, autoOptions);

        google.maps.event.addListener(fromAuto, 'place_changed', function () {
            // var place = fromAuto.getPlace();
            var location = compilePlace(fromAuto.getPlace());
            document.getElementById('pickup_street').value = location.street;
            document.getElementById('pickup_city').value = location.city;
            document.getElementById('pickup_state').value = location.state;
            document.getElementById('pickup_country').value = location.country;
            document.getElementById('pickup_zipcode').value = location.zipcode;
            document.getElementById('pickup_lat').value = location.lat;
            document.getElementById('pickup_long').value = location.lng;
        });

        if ($("#map_canvas").length > 0) {

            fromAuto.bindTo('bounds', map);

            //Find To location
            var toText = document.getElementById('dropoff'),
                toAuto = new google.maps.places.Autocomplete(toText, autoOptions);

            google.maps.event.addListener(toAuto, 'place_changed', function () {
                // var place = fromAuto.getPlace();
                var location = compilePlace(toAuto.getPlace());
                document.getElementById('drop_street').value = location.street;
                document.getElementById('drop_city').value = location.city;
                document.getElementById('drop_state').value = location.state;
                document.getElementById('drop_country').value = location.country;
                document.getElementById('drop_zipcode').value = location.zipcode;
                document.getElementById('drop_lat').value = location.lat;
                document.getElementById('drop_long').value = location.lng;
            });

            toAuto.bindTo('bounds', map);

            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('directions-panel'));

            // var control = document.getElementById('control');
            // control.style.display = 'block';
            // map.controls[google.maps.ControlPosition.TOP].push(control);
        }
    }

    function calcRoute() {
        var start = document.getElementById('pickup').value;
        var end = document.getElementById('dropoff').value;
        var request = {
            origin: start,
            destination: end,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                computeTotalDistance(response);
            }
        });
    }

    function computeTotalDistance(result) {
        var total = 0,
            myroute = result.routes[0],
            totalPrice;

        for (i = 0; i < myroute.legs.length; i++) {
            total += myroute.legs[i].distance.value;
        }
        total = total / 1000;
        $(".fare_distance").val(total.toFixed(2) + " km");

        totalPrice = ((total * 1.5) + 3.60);
        totalPrice = Math.round(totalPrice / 0.20) * 0.20;
        $(".fare_price").data('fare', totalPrice);
        if ($(".fare_price").hasClass('is--minivan')) {
            totalPrice += 10;
        }
        $(".fare_price").val("$" + Math.floor(totalPrice - 1).toFixed(2) + " - $" + Math.ceil(totalPrice + 3).toFixed(2));
    }

    function auto() {
        var input = document.getElementById(('pickup'), ('dropoff'));
        /*   var types
        var options = {
        types: [],
        componentRestrictions: {country: ["AUS"]}
        };*/
        var autocomplete = new google.maps.places.Autocomplete(input, options);
    }
})(jQuery);